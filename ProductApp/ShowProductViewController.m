//
//  ShowProductViewController.m
//  ProductApp
//
//  Created by Chris Dillard on 4/4/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import "ShowProductViewController.h"
#import "ProductStore.h"
#import "ProductDetailViewController.h"

@interface ShowProductViewController ()

@end

@implementation ShowProductViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    DLog(@"view will appear");
    self.products= [[ProductStore sharedInstance] selectAll];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.products count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ProductCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text =[[self.products objectAtIndex:indexPath.row] objectForKey:@"name"];
    return cell;
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showProductDetail"]) {
        ProductDetailViewController *productDetailViewController = segue.destinationViewController;
        productDetailViewController.identifier =[[self.products objectAtIndex:[self.tableView indexPathForSelectedRow].row] objectForKey:@"id"];
    }
}

@end
