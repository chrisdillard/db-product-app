//
//  CreateProductViewController.h
//  ProductApp
//
//  Created by Chris Dillard on 4/4/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import "ViewController.h"

@interface CreateProductViewController : UIViewController

-(IBAction)createProduct:(id)sender;

@end
