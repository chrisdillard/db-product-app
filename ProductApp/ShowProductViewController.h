//
//  ShowProductViewController.h
//  ProductApp
//
//  Created by Chris Dillard on 4/4/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import "ViewController.h"

@interface ShowProductViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

/**
 local store of products for ui
 */
@property NSMutableArray* products;

@end
