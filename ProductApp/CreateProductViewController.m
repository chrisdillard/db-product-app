//
//  CreateProductViewController.m
//  ProductApp
//
//  Created by Chris Dillard on 4/4/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import "CreateProductViewController.h"
#import "ProductStore.h"

@interface CreateProductViewController ()

@end

@implementation CreateProductViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark button selector
-(IBAction)createProduct:(UIButton*)sender{
    [[ProductStore sharedInstance] insertProduct:(int)sender.tag];
    UIAlertView* v = [[UIAlertView alloc] initWithTitle:@"Alert" message:[NSString stringWithFormat:@"Product %d created",(int)sender.tag] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [v show];
}

@end
