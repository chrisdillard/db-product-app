//
//  ViewController.m
//  ProductApp
//
//  Created by Chris Dillard on 4/1/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIButton selectors
-(IBAction)showProduct:(id)sender{
    DLog(@"show product");
    
}

-(IBAction)createProduct:(id)sender{
    DLog(@"create product");
    //push to create product ux
}

@end
