//
//  ProductDetailViewController.m
//  ProductApp
//
//  Created by Chris Dillard on 4/4/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "ProductImageViewController.h"
#import "ProductStore.h"

@interface ProductDetailViewController ()

@end

@implementation ProductDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //select identifier from model
    DLog(@"show product with id = %@",self.identifier);
    
    self.product=[[[ProductStore sharedInstance] selectProduct:self.identifier] mutableCopy];
    
    [self.scrollView setContentSize:CGSizeMake(320, 1000)];
    
    [self.name setText:[self.product objectForKey:@"name"]];
    [self.description setText:[self.product objectForKey:@"description"]];
    [self.image setImage:[UIImage imageNamed:[self.product objectForKey:@"image"]]];
    [self.regPrice setText:[self.product objectForKey:@"regPrice"]];
    [self.salePrice setText:[self.product objectForKey:@"salePrice"]];
    [self.colors setText:[self.product objectForKey:@"colors"]];
    [self.stores setText:[self.product objectForKey:@"stores"]];
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.image addGestureRecognizer:tapRecognizer];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)updateProduct:(id)sender{
    DLog(@"update selected product");
    
    NSDictionary *newProduct = @{@"id" : self.identifier, @"name" : [self.name text], @"description" : [self.description text], @"regPrice": [self.regPrice text] , @"salePrice":[self.salePrice text], @"image":[self.product objectForKey:@"image"], @"colors":[self.colors text], @"stores":[self.stores text]};
    
    [[ProductStore sharedInstance] update:newProduct];
    
    UIAlertView* v = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Product updated" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [v show];
}

-(IBAction)deleteProduct:(id)sender{
    DLog(@"delete selected product");
    [[ProductStore sharedInstance] delete:self.identifier];
    
    UIAlertView* v = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Product deleted" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [v show];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 
#pragma mark keyboard support 

- (void)registerForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}
- (void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGPoint buttonOrigin = self.editingField.frame.origin;
    
    CGFloat buttonHeight = self.editingField.frame.size.height;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    visibleRect.size.height -= [UIApplication sharedApplication].statusBarFrame.size.height;
    visibleRect.size.height -= self.navigationController.navigationBar.frame.size.height;
    visibleRect.size.height -= 20;

    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
        
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}
#pragma mark -
#pragma mark UITextView delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqual:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark -
#pragma mark UITextField delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    self.editingField=textField;
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    DLog(@"text field ended editing");
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showDetailImage"]) {
        ProductImageViewController *productDetailViewController = segue.destinationViewController;
        productDetailViewController.identifier =[self.product objectForKey:@"id"];
    }
}
- (void)tapAction:(UITapGestureRecognizer *)tap{
    [self performSegueWithIdentifier:@"showDetailImage" sender:self];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self registerForKeyboardNotifications];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [self deregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
    
}

@end
