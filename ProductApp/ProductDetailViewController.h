//
//  ProductDetailViewController.h
//  ProductApp
//
//  Created by Chris Dillard on 4/4/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>

@property  NSString* identifier;
@property IBOutlet UILabel* name;
@property IBOutlet UITextView* description;
@property IBOutlet UITextField* regPrice;
@property IBOutlet UITextField* salePrice;
@property IBOutlet UIImageView* image;
@property IBOutlet UITextField* colors;
@property IBOutlet UITextField* stores;

@property IBOutlet UIScrollView* scrollView;

@property NSMutableDictionary* product;

@property UITextField* editingField;


-(IBAction)updateProduct:(id)sender;
-(IBAction)deleteProduct:(id)sender;


- (void)registerForKeyboardNotifications;
- (void)deregisterFromKeyboardNotifications;

@end
