//
//  ViewController.h
//  ProductApp
//
//  Created by Chris Dillard on 4/1/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property IBOutlet UITableView *tableview;


-(IBAction)showProduct:(id)sender;

-(IBAction)createProduct:(id)sender;

@end
