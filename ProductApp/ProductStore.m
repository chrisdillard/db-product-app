//
//  ProductStore.m
//  ProductApp
//
//  Created by Chris Dillard on 4/2/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import "ProductStore.h"

@implementation ProductStore

+ (id)sharedInstance {
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

+ (NSString *) applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

- (id)init {
    if ((self = [super init])) {
        NSString* resourcePath=[[ProductStore applicationDocumentsDirectory] stringByAppendingPathComponent:@"productList.sqlite3"];
        
        if (sqlite3_open([resourcePath UTF8String], &_database) != SQLITE_OK) {
            DLog(@"failure to open sqldb");
        }
        
        //ensure products table exists
        const char *sql = "CREATE TABLE IF NOT EXISTS products(id text, name text, description text, priceRegular text, priceSale text, image text, colors text, stores text);";
        sqlite3_stmt *updateStmt = nil;
        
        if(sqlite3_prepare_v2(_database, sql, -1, &updateStmt, NULL) != SQLITE_OK){
            DLog(@"error inserting into db");
        }
        
        if (SQLITE_DONE != sqlite3_step(updateStmt)){
            DLog(@"error");
            DLog(@"%s",sqlite3_errmsg(_database));
        }
        sqlite3_reset(updateStmt);
        sqlite3_finalize(updateStmt);
    }
    return self;
}

-(void)insertProduct:(int)num{
    NSString *str=[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"product%d",num] ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:str];
    NSError *jsonParsingError = nil;
    
    NSDictionary *product = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonParsingError];
    if (!jsonParsingError){
        DLog(@"product is");
        [self insert:product];
    }
}
#pragma mark -
#pragma mark sql transaction methods
-(void) insert:(NSDictionary*)product{
    
    const char *sql ="INSERT INTO products (id, name, description, priceRegular, priceSale, image, colors, stores) VALUES (?,?,?,?,?,?,?,?)";
    
    sqlite3_stmt *updateStmt = nil;
    
    if(sqlite3_prepare_v2(_database, sql, -1, &updateStmt, NULL) != SQLITE_OK){
        DLog(@"error inserting into db");
        return;
    }

    sqlite3_bind_text(updateStmt, 1, [[product objectForKey:@"id"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 2, [[product objectForKey:@"name"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 3, [[product objectForKey:@"description"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 4, [[product objectForKey:@"regPrice"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 5, [[product objectForKey:@"salePrice"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 6, [[product objectForKey:@"image"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 7, [[product objectForKey:@"colors"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 8, [[[product objectForKey:@"stores"] description] UTF8String], -1, NULL);

    if (SQLITE_DONE != sqlite3_step(updateStmt)){
        DLog(@"error");
        DLog(@"%s",sqlite3_errmsg(_database));
        return;
    }
    sqlite3_reset(updateStmt);
    sqlite3_finalize(updateStmt);
}

-(void) delete:(NSString*)identifier{
    const char *sql = [[NSString stringWithFormat:@"DELETE FROM products WHERE id ='%d'",[identifier intValue]] UTF8String];
    
    sqlite3_stmt *deleteStatement=nil;

    sqlite3_prepare_v2(_database, sql, -1, &deleteStatement, NULL);
    if (sqlite3_step(deleteStatement) == SQLITE_DONE)
    {
     
    } else {
        DLog(@"%s",sqlite3_errmsg(_database));
        DLog(@"error deleting ");
    }
    sqlite3_finalize(deleteStatement);
}

-(void) update:(NSDictionary*)product{
    
    sqlite3_stmt *updateStmt = nil;
    
    const char *sql = [[NSString stringWithFormat:@"UPDATE products set id = ?, name = ?, description = ?, priceRegular = ?, priceSale = ?, image = ?, colors = ?, stores= ? WHERE id ='%d'",[[product objectForKey:@"id"] intValue]] UTF8String];
    
    if(sqlite3_prepare_v2(_database, sql, -1, &updateStmt, NULL) != SQLITE_OK){
        DLog(@"error inserting into db");
        return;
    }
    
    sqlite3_bind_text(updateStmt, 1, [[product objectForKey:@"id"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 2, [[product objectForKey:@"name"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 3, [[product objectForKey:@"description"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 4, [[product objectForKey:@"regPrice"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 5, [[product objectForKey:@"salePrice"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 6, [[product objectForKey:@"image"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 7, [[product objectForKey:@"colors"] UTF8String], -1, NULL);
    sqlite3_bind_text(updateStmt, 8, [[[product objectForKey:@"stores"] description] UTF8String], -1, NULL);
    
    if (SQLITE_DONE != sqlite3_step(updateStmt)){
        DLog(@"error");
        DLog(@"%s",sqlite3_errmsg(_database));
        return;
    }
    sqlite3_reset(updateStmt);
    sqlite3_finalize(updateStmt);
}

-(NSDictionary*) selectProduct:(NSString*)identifier{
    sqlite3_stmt *selectStatment = nil;
    const char *sql = [[NSString stringWithFormat:@"SELECT * FROM products WHERE id ='%d'",[identifier intValue]] UTF8String];
    if(sqlite3_prepare_v2(_database, sql, -1, &selectStatment, NULL) != SQLITE_OK ){
        DLog(@"%s",sqlite3_errmsg(_database));

        DLog(@"error selecting product from db");
        return nil;
    }
    
    int ret = sqlite3_step(selectStatment);
    
    if (ret!=100){
        DLog(@"%s",sqlite3_errmsg(_database));
        
        DLog(@"error selecting product from db");
        return nil;
    }
    
    NSString* identifier2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 0)];
    NSString* name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 1)];
    NSString* desc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 2)];
    NSString* regPrice = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 3)];
    NSString* salePrice = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 4)];
    NSString* image = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 5)];
    NSString* colors = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 6)];
    NSString* stores = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 7)];
    
    NSDictionary *item = @{@"id" : identifier2, @"name" : name, @"description" : desc, @"regPrice": regPrice , @"salePrice":salePrice, @"image":image, @"colors":colors, @"stores":stores};

    sqlite3_reset(selectStatment);
    sqlite3_finalize(selectStatment);

    
    return item;
}
-(NSMutableArray*)selectAll{
    sqlite3_stmt *selectStatment = nil;
    const char *sql = "SELECT * FROM products";
    if(sqlite3_prepare_v2(_database, sql, -1, &selectStatment, NULL) != SQLITE_OK ){
        DLog(@"error selecting all from db");
        return nil;
    }
    NSMutableArray *retArr = [[NSMutableArray alloc] init];
    while (sqlite3_step(selectStatment) == SQLITE_ROW) {
        NSString* identifier = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 0)];
        NSString* name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 1)];
        NSString* desc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 2)];
        NSString* regPrice = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 3)];
        NSString* salePrice = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 4)];
        NSString* image = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 5)];
        NSString* colors = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 6)];
        NSString* stores = [NSString stringWithUTF8String:(char *)sqlite3_column_text(selectStatment, 7)];

        DLog(@"selected a row");
        NSDictionary *item = @{@"id" : identifier, @"name" : name, @"description" : desc, @"regPrice": regPrice , @"salePrice":salePrice, @"image":image, @"colors":colors, @"stores":stores};

        [retArr addObject:item];
    }
    sqlite3_reset(selectStatment);
    sqlite3_finalize(selectStatment);

    return retArr;
}

- (void)dealloc {
    sqlite3_close(_database);
}
@end
