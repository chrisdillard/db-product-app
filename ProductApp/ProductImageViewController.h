//
//  ProductImageViewController.h
//  ProductApp
//
//  Created by Chris Dillard on 4/4/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductImageViewController : UIViewController

@property  NSString* identifier;

@property IBOutlet UIImageView* image;

@end
