//
//  AppDelegate.h
//  ProductApp
//
//  Created by Chris Dillard on 4/1/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
