//
//  ProductStore.h
//  ProductApp
//
//  Created by Chris Dillard on 4/2/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface ProductStore : NSObject

/**
 sql database
 */
@property (nonatomic) sqlite3 *database;

/**
 create singleton for db transaction
 */
+ (id)sharedInstance;

/**
 where to save db
 */
+ (NSString *) applicationDocumentsDirectory;
/**
 insert product into db
 */
-(void)insertProduct:(int)num;
/**
 select specific db record
 */
-(NSDictionary*) selectProduct:(NSString*)identifier;

/**
 update specific db record
 */
-(void) update:(NSDictionary*)product;

/**
 delete a product record
 */
-(void) delete:(NSString*)identifier;
/**
 select all product records and return
 */
-(NSMutableArray*)selectAll;

@end
