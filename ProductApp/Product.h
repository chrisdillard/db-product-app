//
//  Product.h
//  ProductApp
//
//  Created by Chris Dillard on 4/1/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

/**
product unique identifier
*/
@property (nonatomic) NSString *identifier;
/**
 product name
 */
@property (nonatomic) NSString *name;
/**
 description text
 */
@property (nonatomic) NSString *description;
/**
 regular price
 */
@property (nonatomic) NSDecimalNumber *priceRegular;
/**
 sale price
 */
@property (nonatomic) NSDecimalNumber *priceSale;
/**
 photo in app bundle
*/
@property (nonatomic) NSString *photo;
/**
 colors
 */
@property (nonatomic) NSMutableArray *colors;
/**
 stores
 */
@property (nonatomic) NSMutableDictionary *stores;
@end
