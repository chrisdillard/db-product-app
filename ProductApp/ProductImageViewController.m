//
//  ProductImageViewController.m
//  ProductApp
//
//  Created by Chris Dillard on 4/4/14.
//  Copyright (c) 2014 macys. All rights reserved.
//

#import "ProductImageViewController.h"
#import "ProductStore.h"

@interface ProductImageViewController ()

@end

@implementation ProductImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImage* newImage =[UIImage imageNamed:[[[ProductStore sharedInstance] selectProduct:self.identifier]  objectForKey:@"image"]];
    [self.image setImage:newImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
