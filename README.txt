Hi Manager, 

This app uses sqlite3 to implement the model described in the document I received. 

Supported DB transactions are insert,delete,select,update. 

All DB transactions are handled through the ProductStore module. The sqlite3 DB is stored in the apps Documents directory and persists across app runs. 

The app consists of a UI Module including a Storyboard and various UIViewControllers.
UIViewControllers:
	-CreateProductViewController
	-ProductdetailViewController
	-ProductImageViewController
	-ShowProductViewController

The UI supports:
	-Create Product (Create from JSON templates in "Product Data" folder.
		-Product 1
		-Product 2
		-Product 3
	-Show Product: 
		-Select Created Product:
			-Update Product
			-Delete Product
			-Tap Product image to full screen it.
			
			
Thanks,

Chris Dillard
	 